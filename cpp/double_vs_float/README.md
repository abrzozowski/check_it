# Double vs Float

“The exact meaning of single-, double-, and extended-precision is implementation-defined. Choosing the right precision for a problem where the choice matters requires significant understanding of floating-point computation. If you don’t have that understanding, get advice, take the time to learn, or use double and hope for the best.” The C++ Programming Language, Bjarne Stroustrup

There are three floating point types in C and C++:

* float (single-precision)
* double (doube-precision)
* long double (extended-precision)

CPU model name: Intel(R) Core(TM) i5-4210M CPU @ 2.60GHz, memory: 16113772 kB

    Type name: e Size in bytes: 16 Summation time in s: 7844
    summed value: 6.6e+09
    Type name: d Size in bytes: 8 Summation time in s: 5785
    summed value: 6.6e+09
    Type name: f Size in bytes: 4 Summation time in s: 5793
    summed value: 6.71089e+07

    Type name: e Size in bytes: 16 Summation time in s: 7826
    summed value: 2.22e+09
    Type name: d Size in bytes: 8 Summation time in s: 5784
    summed value: 2.22e+09
    Type name: f Size in bytes: 4 Summation time in s: 5825
    summed value: 3.35544e+07

    Type name: e Size in bytes: 16 Summation time in s: 7886
    summed value: 1.998e+10
    Type name: d Size in bytes: 8 Summation time in s: 5840
    summed value: 1.998e+10
    Type name: f Size in bytes: 4 Summation time in s: 5999
    summed value: 2.68435e+08

Depends on what the native hardware does [[2][2]]

* If the hardware implements double (like the x86 does), then float is emulated by extending it there, and the conversion will cost time. In this case, double will be faster.
* If the hardware implements float only, then emulating double with it will cost even more time. In this case, float will be faster.
* And if the hardware implements neither, and both have to be implemented in software. In this case, both will be slow, but double will be slightly slower (more load and store operations at the least).

There is exactly one case where you should use float instead of double [[1][1]]

* It’s smaller. On 64bit hardware with a modern gcc, double is 8 bytes and float is 4 bytes.

Similarly, there is exactly one time you would need to use long double. [[1][1]]

* It’s very high precision. You might have an application that needs the absolutely most correct floating point precision you can get today.

[1]: http://articles.emptycrate.com/2012/02/11/double_vs_float_which_is_better.html
[2]: https://stackoverflow.com/questions/4584637/double-or-float-which-is-faster