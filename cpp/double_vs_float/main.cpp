// based on http://articles.emptycrate.com/2012/02/11/double_vs_float_which_is_better.html

#include <chrono>
#include <vector>
#include <iostream>
#include <typeinfo>

template<class T>
T sum(int num_times, T value) {
    T val = 0;

    std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < num_times; ++i)
    {
        val += value;
    }
    std::chrono::high_resolution_clock::duration d = std::chrono::high_resolution_clock::now() - start;

    std::cout << "Type name: " << typeid(T).name() << " Size in bytes: " << sizeof(T) << " Summation time in ms: "
              << std::chrono::duration_cast<std::chrono::milliseconds>(d).count() << std::endl;

    return val;
}

int main() {
    std::cout << " summed value: " << sum(2000000000, 3.3L) << std::endl;
    std::cout << " summed value: " << sum(2000000000, 3.3 ) << std::endl;
    std::cout << " summed value: " << sum(2000000000, 3.3f) << std::endl;
    std::cout << std::endl;
    std::cout << " summed value: " << sum(2000000000, 1.11L) << std::endl;
    std::cout << " summed value: " << sum(2000000000, 1.11 ) << std::endl;
    std::cout << " summed value: " << sum(2000000000, 1.11f) << std::endl;
    std::cout << std::endl;
    std::cout << " summed value: " << sum(2000000000, 9.99L) << std::endl;
    std::cout << " summed value: " << sum(2000000000, 9.99 ) << std::endl;
    std::cout << " summed value: " << sum(2000000000, 9.99f) << std::endl;
}